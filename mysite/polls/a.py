# coding=utf-8
n = int(raw_input())
d = map(int, raw_input().split())
d.sort(reverse=True)

k = 0
c = 1 << 33
for x in d:
    if c > 0:
        c = min(c - 1, x)
        k += c
print k



